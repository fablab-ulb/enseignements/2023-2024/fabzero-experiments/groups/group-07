# Group and project presentation

## About our team

Our team is composed of :

- [Alejandro Montero](https://alejandro-montero-fablab-ulb-enseignements-2023--3a253f28b22529.gitlab.io/)
- [Edleby Oubayda](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/template/)
- [Emilie Cellier](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/template/)
- [Théo Grijspeerdt](https://theo-grijspeerdt-fablab-ulb-enseignements-2023-2-966d6998506ad4.gitlab.io/)


Here we are :

![](images/WhatsApp%20Image%20yay.jpg)


Our team in composed (from left to right) of Alejandro(civil-engineer), Théo(bio-engineer), Emilie(bio-engineer) and Oubayda(bio-engineer).

## Our Project abstract

Our problematic is the illumination of a workstation(a table) in Lebanon,using a system that is independent of the public electricity network. The first solution we prototyped was a low-tech version of a gravity lamp. The idea behind the gravity lamp is quite clever: a weight is attached to a rope and is elevated. When the weight falls, the gravitational force is transformed into mechanical energy by turning a pulley that is connected to a gear system. The gear system is connected to a dynamo, that converts the mechanical energy into electrical energy to power a LED. This system produce light using potentiel energy as an input. Unfortinately, the weight needs to be lifted to frequently to be praticle at a workstation. Our final project consisted in modifying a standard flashlight, equipped with a dynamo activated by pressing a lateral triangle. The goal is to transform this flashlight into a foot-activated lamp. The concept involves triggering the light through foot pressure on a pedal, which in turn operates the lateral triangle on the flashlight. This action will activate a LED connected to the dynamo, strategically positioned on a lamp-arm structure for effective illumination. Such a system allows the user to create light while working.


![Graphical abstract](images/firstname.surname-slide.png)

![](./images/final2.jpeg)

