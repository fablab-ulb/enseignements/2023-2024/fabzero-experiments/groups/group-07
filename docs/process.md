# Processus 

## <span style="color:orange;">1.Mise en place de la structure des réunions,construction des arbres et définition de la problématique et de la solution (06/11,07/11,13/11,14/11)</span>

### <span style="color: turquoise;">1.1.Outils de groupe</span>


Nous avons eu une présentation nous donnant quelques outils de gestion de groupe. Parmis ces outils, ceux que nous avons sélectionnés sont détaillés ici.

#### 1.1.2.Charte 

Le premier outil que nous avons implémenté est la charte. La chate est construite pour nous permettre de travailler dans un cadre qui nous convient. Chacun a pu proposer des éléments qui sont important pour lui lorsqu'il travaille en groupe. Certains avaient eu des problèmes dans des travaux de groupe précédents et voulaient éviter que ces problème ne se reproduisent. Nous nous sommes concerté et voici les règles sur lesquelles nous nous sommes mis d'accord : 

1.	**Respect des deadline**
2.	**Aller à l’essentiel**
3.	**Ne pas négliger les idées des membres du groupe**
4.	**Appréciation de la réunion avec la technique des doigts**

#### 1.1.3.Rôles dans le groupe 
Il nous a également été conseillé de désigner des rôles à chacun pour que la réunion se déroule au mieux. Parmis ces rôles figuraient les rôles suivant : 

- Le modérateur, qui va faire en sorte que les sujets nécéssaires soient abordés lors de la réunion, rappeler les tâches etc...
- Le secrétaire, qui s'occupera de prendre note et de rédiger la documentation liée.
- Le maitre du temps, qui va mettre les limites de temps, rappeler le temps qu'il reste pour la réalisation d'une tâche etc...
- L'ambianceur, qui a pour rôle de prendre la température de chacun après chaque réunion, sur comment il s'est senti etc...

Pendant les premières réunions, nous avons décidé de changer de rôle à chaque fois. De cette manière, tout le monde à pu essayer tous les rôles. Après quelques réunions, nous avons trouvé une dynamique de groupe qui nous a permis de travailler ensemble sans définir de rôles aussi formatés que précédement.

#### 1.1.4.Ambiance

Après chaque réunion, nous faisions un "temperature check" pour déterminer la satifsfaction du groupe par rapport à la réuinon. Cela permet de voir comment chaque membre du groupe s'est senti lors de la réunion. Ca peut paraitre anondin mais nous pensons que c'est une bonne chose sur le long terme. Un pouce en l'air pour dire que ca s'est bien passé, un pouce au milieu pour dire bof, et un pouce en bas pour dire que c'était catastrophique. Ceci permet d'identifier rapidement si quelqu'un à un problème avec quelque chose. A la prochaine réunion, on fesait attention à bien respecter les défauts qui avaient été soulevés lors de la réunion précédente.

### <span style="color: turquoise;">1.2.Rassemblement des informations pour le groupe</span>


Une idée qui a été proposée était de créer un Word partagé, dans lesquel on allait mettre toutes les évolutions des solutions, des problématiques, des notes de groupes etc... pour pouvoir avoir un espace partagé dans lequel toutes les informations soient rassemblées au même endroit. Ainsi, si quelqu'un n'a pas été attentif durant un moment à une réunion, ou qu'il était absent, ou tout simplement s'il veut un petit rappel concernant l'issue du projet, il n'a qu'à ouvrir le Word. Chaque réunion est recensée avec la date, la charte, l'objectif de la réunion et la retranscription des discussion et des décisions prises pendant ces réunions. 

Nous avons aussi utilisé un groupe whatsapp pour communiquer entre nous. Celui-ci nous a permis de trouver des dates pour les réunions, nous organiser lorsque plusieurs personnes devaient travailler au FabLab, etc. On l'a aussi utilisé pour organiser quelques réunions à distance et nous organiser lorsqu'une réunion en présentiel n'était pas possible. 

### <span style="color: turquoise;">1.3.Construction de l'arbre à problèmes</span>

Avant cette séance, nous avions du amener un objet en lien avec une problématiquer personnelle. Nous avons pu former des groupes en rassemblant les étudiants qui vaient des problématiques similaires ou qui partageaient des thèmes. Lors de cette précédente séance, nous avions déjà réfléchis à différent sthèmes que nous voulions aborder ensemble. Nous avions obtenus ce résultat:
![](./images/Image1.jpg) 


De tous les thèmes abordés, nous avons choisis de nous concentrer sur une seule problématique lors de cette réunion. Le thème global que l'on a choisi était **"L'énergie"**, plus précisément, le problème était **l'utilisation de sources d'énergies non-écologiques.** 

En partant de ce thème très général, on a défini plusieurs autres problèmes et leurs causes et nous les avons mis sur une feuille A4 pour former notre arbre des problèmes. Voici le résultat que nous avons obtenu: 

![](./images/Arbre%20a%20problème%201.jpg)

![](./images/arbre%20a%20probleme%202.jpg)

On constate directement la présence des deux premières racines : "intermittance" et "accessibilité à l'énergie". <br>
Par intermittance, on voulait mettre en avant le fait que nos habitudes de consommation d'énergie ne prennent pas en compte la source de l'énergie électrique. Ainsi, certains types de consommation (chargement de batteries, chauffage de ballon d'eau,...) pourraient être synchronisés avec la production d'électricité ppar des sources d'énergies renouvelables.<br>

Par accessibilité à l'énergie, c'est tout simplement le fait qu'il existe encore des pays défavorisés et/ou en voie de développement qui n'ont pas un accès à l'électricité régulier, stable et abordable économiquement.

Denis nous a conseillé de cibler une problématique beaucoup plus précise et que pour ce faire il fallait s'imposer des contraintes fortes. Qu'entend on par "contraintes" ? Ce sont tout simplement des limittes (un cadre) que l'on va s'imposer pour nous forcer à affiner notre problématique plus précisément. On a alors poussé les choses beaucou plus loin. Oubayda avait donné comme problématique le manque d'accessibilité à l'électricité au Liban, à Tripoli. On s'est alors demandé pourquoi avions-nous besoin d'éléctricité ? Même si aux premiers abords, celà peut paraitre évident, il s'avère que ca nous a facilité la définition de notre problématique. 

Nous avions donc déjà réussi à créer des contraintes : 

- Le lieu : Tripoli au Liban
- Quoi ? : L'électricité
- L'éléctricité pour quoi ? : Bouilloire, lumière, frigo, ascenceur...

En s'imposant ses contraintes, on arrivait à du concret. La bouilloire pour pouvoir faire du thé, c'est essentiel. Un frigo, pour pouvoir stocker la nourriture, c'est essentiel aussi. La lumière, pour travailler ou lire, également. 


### <span style="color: turquoise;">1.4.Constructions de l'arbre à solutions</span>


Des solutions ont alors commencé à émerger. Il faut quand même préciser que l'idée de base d'un des membres du groupe était de créer un dispositif qui aurait permis de gérer l'intermittance énergétique. En gros, il serait relié à un site qui fournit les informations énergetiques en temps réel. Ce site fournit la quantité d'énergie en kWh issu de production renouvelable. Et le but était de créer un dispositif que l'on branche à sa prise et qui permet de recharger ses appareils uniquement quand le rapport énergie renouvelable/fossile est favorable. Ceci aurait en fait permis de n'utiliser de l'électricité que quand elle était majoritairement issue d'énergies renouvelables. De cette manière le mix électrique utilisé pour charger nos appareil aurait été plus vert que le mix électrique moyen belge.
On avait donc fait "l'erreur", qui est réccurente, de commencer par la solution à la place de commencer par trouver la solution, avant même de définir la problématique. 

D'autres solutions identifiées concernaient le frigo, la bouilloire, la récupération et le stockage d'énergie perdue dans les freins de vélo dans une batterie (au vu de l'abondance de vélo/cyclomoteurs au Liban). Pour la bouilloire, le four solaire était une des solutions, et la solution du frigo était de rajouter une isolation autour du frigo, ou bien de compartimenter le frigo pour limiter les pertes de chaleur.
Pour choisir une solution parmis toutes celles identifiées, le but était de contacter des mentors chacun de son côté pour voir si la solution peut être implémentée, réalisée, quels seront les problèmes qui y seront liés etc...
Chacun s'était vu attribué une solution en particulier. Durant la réunion où en a discuté, on s'est rendu compte que la solution de récupération d'énergie perdue lors du freinage était beaucoup trop compliquée. La solution du dispositif contre l'intermittance était trop technique. Le four solaire ne nous excitait pas tant que ca. En ce qui concerne le frigo, améliorer un frigo est intéressant quand on a de l'électricité pour le faire tourner. Quand il n'y a pas assez d'électricité pour qu'il fonctionne normalement c'est un peu inutile.

Il fallait donc aller sur quelque chose d'autre et surtout, bien définir la problématique selon Denis.

![](./images/arbre%20a%20solution.jpg)

### <span style="color: turquoise;">1.5.Définition de la problématique</span>


Après avoir rencontré tout ces petits problèmes, la problématique sur la laquelle on s'est accordé, et que Denis a validé est <span style="color:orange"> : *Le manque d’accessibilité à l’électricité au Liban à Tripoli et l’utilisation de source d’énergie non renouvelable.*</span> 


### <span style="color: turquoise;">1.6.L'électricité au Liban </span>
 

Une question qui se posait alors, était l'utilisation de l'électricité au Liban. Qu'est ce qui ne va pas ? Comment ca fonctionne ? Dans quoi est-ce que les habitants l'utilisent ? De quelle manière etc... 
Un des membres du groupe a pour pays d'origine le Liban, il a donc plus ou moins connaissance du problème d'électricité là-bas.
Le Liban est plongé dans une crise énergétique depuis plusieurs années maintenant. La distribution d'énergie à travers le pays n'est jamais constante. Les habitants doivent d'office payer cette électricité mais sans jamais être sûr qu'elle sera disponible. L'état libanais peut par exemple un jour fournir 2h d'électricité dans la journée, parfois 3, 4, 5, parfois pas du tout. Il est parfois arrivé que l'électricité ne soit achemeninée dans les foyers libanais qu'une seule fois dans le mois. 

Les habitants doivent alors se reposer sur d'autres solutions. Parmis celle-ci, il y a l'utilisation de grand générateurs, mais qui sont disponibles pour les habitants à des prix exhorbitants. Pour avoir une échelle de grandeur, les habitants doivent payer en moyenne un peu plus de 100 $ par mois. Sachant que le salaire moyen d'un libanais vivant à Tripoli en 2023 est de 320.87 euros, cela représente un poste de dépense important pour les ménages. 

De plus, l'électricité n'est pas disponible toute la journée. Il y a un système défini. Les génerateurs vont être allumés tôt le matin pour pouvoir faire monter l'eau dans les grands batiments via un système de pompe. Pour que la pompe puisse monter l'eau dans les différents appartements du quartier, il faut évidemment lui fournir de l'électricité. C'est donc une partie de la journée durant laquelle l'électricité génerée par les générateurs va être utilisée. 

En tout, il y a des fois 8, des fois 12 h d'électricité dans la journée fournie par les génerateurs. 

Il y a également des monopoles et des mafias qui s'organisent autour des générateurs qui rentrent en jeu mais c'est beaucoup plus complexe que ca donc on ne va pas rentrer dans les détails. 

La "solution" utilisée par la majorité des libanais est l'utilisation des panneaux solaires. Le Liban étant un pays méditerannéen, le pays est dans la globalité, ensoleillé toute l'année, ce qui permet aux libanais de vivre un peu plus décemment. Mais il existe également une inégalité des classes. Il y a des gens pour qui celà va être facile d'installer plus de panneaux solaires, d'autres pour qui ca va être difficile, et d'autres pour qui celà va être impossible par manque de moyen. Les gens qui ont beaucoup d'argent peuvent par exemple se permettre d'acheter des batteries au Lithium, ou disposer sur leurs toit plus d'une dizaine de panneaux solaires, ce qui est loin d'être le cas de tout le monde. 

Avec les panneaux solaires, lorsqu'il y a une forte intensité lumineuse seulement, on peut faire tourner la majorité des électroménagers tel que les frigos, bouilloire etc... Mais dés lors que l'intensité lumineuse diminiue, il devient plus difficile de génerer assez d'électricité mais encore une fois tout dépend du nombre de panneaux solaires installés. Au plus il y en a, au plus la puissance électrique génerée sera importante. 

Pendant la journée, l'énergie excédentaire va être accumulée dans des batteries, qui vont pouvoir être utilisées pour les besoins prioritaires tels que la lumière ou avoir une heure de télevision. 

Pour ce qui est de l'eau chaude, ils disposent d'un chauffeur fonctionnant à l'énergie solaire. 

Sources des informations: Oubayda, [Human rights watch](https://www.hrw.org/fr/news/2023/03/09/liban-la-crise-de-lelectricite-exacerbe-la-pauvrete-et-les-inegalites)

### <span style="color: turquoise;">1.7.Définition de la solution</span>


L'idée est de créer une solution low tech. La solution à laquelle on a pensé était la géneration de lumière. Le domaine de l'énergie et de l'électricité en géneral est très imperméables avec beaucoup de systèmes complexes et déjà très efficace. Il nous est donc compliqué de les améliorer. Prenons l'exemple de l'ascenceur, il va être très compliqué de fabriquer un ascenceur fonctionnant en low tech.  Pareil pour le frigo, créer un frigo est une solution difficile à implémente. 

L'idée de la lumière était donc plus "atteignable". 

Notre solution adhère a 5 des objectifs des [17 Sustainable Development Goals](https://sdgs.un.org/), notamment:

4 Quality education​

7 Affordable and clean energy​

11 Sustainable cities and communities​

12 Responsible consumption and production​

13 Climate Action​

​
## <span style="color:orange;">2.Entretien avec Denis,Jonathan Vigne,recherche des dimensionnements nécessaires à la conception de la gravity lamp,et premiers prototypes(28/11 et 5/12)</span>

Cette semaine étais un peu particulière car on était a chaque fois deux personnes pour faire avancer le projet…La réunion c’est déroulée en 3 parties :
### <span style="color: turquoise;">2.1 Début du projet </span>


En cours de projet, nous avons eu un entretien avec Denis qui nous a permis de mettre au clair certaines de nos idées, nous donner des pistes, et  nous mettre en garde par rapport à certains aspects de notre projet.
Les points essentiels du rendez-vous peuvent être résumés ci-dessous :

1. Avant de faire quoi que ce soit de concret( modulage d’engrenages,prototypes,etc), le mieux est de faire tout le dimensionnement physique  de sorte à ce que l’on peux justement ajuster nos résultats à nos prototypes (si on fait tomber un poids de tel kg, on va pouvoir faire fonctionner une LED avec telle puissance durant autant de temps ,quelle puissance la LED devrait avoir pour éclairer une table pour lire,etc)

2. Il faut vraiment plonger dans le monde des Libanais et vraiment se mettre dans leur situation, pour moduler notre solution au mieux

3. Il faut faire attention à certaines solutions « miracles » que l’on pourrait trouver sur internet. La plupart du temps ce sont des scams et sont là pour que acheter des choses qui ne marchent pas réellement. La gravity lamp pourrait être une invention de ce type.

4. Denis nous a proposé deux manières d’aborder l’élaboration de notre prototype : soit on essaye la gravity lamp et on paufine cette solution progressivement.
Soit on commence plusieurs petits prototypes avec la création de lumière comme thème principal. Il nous a donné quelques idées comme cette [lampe munie d’un chargeur USB utilisant des cellules de lithium sur des batteries d’ordinateurs portables usagées](https://wiki.lowtechlab.org/wiki/Lampe_solaire_%C3%A0_batteries_lithium_r%C3%A9cup%C3%A9r%C3%A9es)

5. Des mentors intéressants pour faire les engrenages de la gravity lamp seraient Jonathan Vigne et Axel Cornu

### <span style="color: turquoise;">2.2 Entretien avec Jonathan Vigne :</span>


On lui a montré nos idées d’engrenages et il a dis que c’était bien mais dans un premier temps ce serait bien d’essayer d’allumer une led avec que [le système de thinkgivers dont on s'est inspirés](https://www.thingiverse.com/thing:1727833 ) pour voir ce que ca fait(tourner la manivelle + connecter les engrenages a la dynamo, sans mettre de poids dans un premier temps)

Il nous a donné une idée de rajouter une petite batterie pour stocker de l’energie pour alimenter encore + lampe après


### <span style="color: turquoise;">2.3 Dimensionnement nécessaire à la conception d'une gravity lamp :</span>


•	<span style="color: lightgreen;">Cahier des charges </span>


o	1 table = 1m² = 10sqft ([source](https://www.ecosia.org/search?q=square%20foot%20in%20m%C2%B2&addon=opensearch))

o	10sqft nécessitent 700 lumen pour un lieu de travail ([source](https://www.lightingtutor.com/how-many-lumens-do-i-need/#:~:text=Study%20Room%20or,to%2080%20lumens ))

o	Besoin de 8.5 watts pour produire 700 lumen ([source](https://www.amazon.com/Equivalent-Daylight-Medium-Non-Dimmable-Listed/dp/B081ZJR6M2/ref=sr_1_4?c=ts&keywords=LED%2BBulbs&qid=1701363967&s=hi&sr=1-4&ts_id=2314207011&th=1#:~:text=Technical%20Features%20%2D%2D%2D%2024%20Packs%2C%20A19%20LED%20bulb%2C%208.5%20watts%2C%20750%20lumens%2C))

•	<span style="color: lightgreen;">En partant de l’énergie potentielle  </span>

o	8.5 watt = 8.5 J/s On vise 10J/s pour considérer les frottements et pertes. 

o	On veut produire pendant 20min sans interruption = 60s*20=1200s 

o	Pour produire 10J/s pendant 1200s, il faut initialement 12000J

o	Pour produire 12000J, il faut descendre 20kg sur 61m ([source](https://www.omnicalculator.com/physics/potential-energy?c=EUR&v=g:1!g!l,PE:12000!J!l,mass:10!kg  ))

o	L’amplitude verticale etant de 1m, il faut un rapport d’engrenages de 1:60 à la dynamo soit 0.0166 ([source](https://www.omnicalculator.com/physics/gear-ratio?c=EUR&v=mechanicalAdvantage:60!!l   )). Cette étape contient une erreur qui sera détaillée plus tard. Cependant, le reste du raisonnement est laissé pour illustrer notre erreur.

o	Si le poids est fixé sur un engrenage de 10 dents, 3 engrenages dont le grand rayon fait 30 dents et le petit fait 10 dents, et la dynamo est liée à un engrenage de 30 dents, on obtient un ratio total de 0.0123.([soucre](https://evolventdesign.com/pages/gear-ratio-calculator ))

o	Si le poids descend de 1.5m en 1200s, il va a 0.0125m/s ([source](https://www.unitconverters.net/speed-converter.html#:~:text=1.5/1200%20meter/second%20%3D%200.00125%20meter/second ))

o	Pour un engrenage de 10 dents, le diamètre est de 12cm ([source](https://www.thingiverse.com/thing:2386318 ))

o	Le périmetre est de 37.669cm ([source](https://www.omnicalculator.com/math/circle-perimeter?c=EUR&v=hide:0,d:12!cm ))

o	A 0.0125m/s=1.25cm/s. En 60 secondes, cet engrenage fera 75cm donc 2 RPM 

o	La dynamo fera donc 54 RPM ([source](https://geargenerator.com/beta/#XW6WjOJwiOJwiMck6DDBktG7oYrc$p1oulm4k6pD@tPVMNDQejeVkYzI494OwuqawjiOcKQbWhTSj8LaM6l1Gxc3EyDchekEW0  ))

•	<span style="color: lightgreen;">En partant de la connexion LED/dynamo  </span> 

L'utilisation d'une dynamo de vélo implique de bien comprendre le fonctionnement de l'électronique embarquée dans cet objet. En le modifiant légèremen, nous voulions pouvoir utiliser la dynamo de vélo dans notre application. ( [source](https://www.instructables.com/Dynamo-powered-LED-bike-lights/) )

Voici le circuit dont nous avons besoin, c'est a dire le circuit que nous devons construire...

![](./images/circuit%20Alex.jpeg)

Pour construire ce circuit, nous avons besoin des composants suivants.
- 1W power led (luxeon, SSC P4, Cree or something similar) on star PCB - it must withstand at least 500mA current. (it looks like [that](http://commons.wikimedia.org/wiki/Image:2007-07-24_High-power_light_emiting_diodes_(Luxeon,_Lumiled).jpg) )
- Collimator or reflector suited for above power LED with 10 - 30 degrees beam width (10 degrees has visible light-spot in longer range; 30 deg. puts more light to the sides and is more visible for others) 
- 3 (or 4 when You don't make the tail light) Shottky diodes (for example 1N5818) for rectifying (faster, lower voltage drop). the normal silicone diodes will also do. 
- Capacitor C1 - 2200uF 4V 
- Capacitors C2 and C3 470uF 63V 
- Resistor R1 - 47kOhm 
- 1 Longer Bolt and 2 nuts for LED mount/heatsink 
- Plastic tubing for the casing 
- some casing for tail light (I used some clear plastic rail in which integrated circuits are transported) 
- some length of 2 wire cable for connecting dynamo, rectifier, head and tail light
-	Need of ground

### <span style="color: turquoise;">2.4.Fonctionnement d'engrenages
    
Comme aucun d'entre nous n'avait travaillé avec des engrenages précédement, nous avons du nous renseigner sur ceux ci. Nottament le vocabulaire lié aux [engrenages](https://www.youtube.com/watch?v=IBcGLpQnfYk  ), le calcul des [rapports](https://www.youtube.com/watch?v=txQs3x-UN34  ) et les manière de les agencer. L'utilisation d'une boite de vitesse a été envisagée mais comme notre concept devait toruner à vitesse constante pour une charge constante, ce composant n'était pas nécessaire.

Pour éviter les problèmes de frottements liés aux engrenages, il fallait s'assurer de travailler avec un matériaux bien lisse et envisager d'utiliser un lubrifiant dans le méchanisme. L'utilisation de dents d'engrenages avec une forme non rectiligne ([involute thooths](https://www.youtube.com/watch?app=desktop&v=gt_Ofn95ML0 )) permet de renforcer le transfert de force en diminuant les frottement. Il était aussi important d'avoir un méchanisme bien équilibré et des engrenages correctement agencés. Un dernier parametre important est [l'espacement entre les dents](https://www.youtube.com/watch?v=KqkOfOOeoSs) qui doit être correctement ajusté.

Afin d'avoir un meilleur controle sur la vitesse de rotation des engrenages et la descente du poids, nous avons pensé à utiliser un système de freinage. Cependant, la majorité des systèmes envisagés causaient des pertes d'énergie par friction, nottament les [plaquettes de freins](https://www.youtube.com/watch?v=mFOYEBxnLxc ), ou bien étiaent trop complexe à mettre en place, comme ls méchanismes trouvés dans des montres.
 


### <span style="color: turquoise;">2.5.Impression du [ Planetary](https://www.thingiverse.com/thing:1727833 ) et des premiers protoypes d'engrenages</span>


#### Planetary 

Voici le [ Planetary](https://www.thingiverse.com/thing:1727833 ) imprimé. C'est notre premier prototype imprimé. L'avantage de ce système d'engrenage est qu'il permet de supporter des poids importants sans casser. Cependant, le modèle imprimé était beaucoup trop dur à faire tourner à cause des nombreux frottements. Nous avons donc abandonné ce type d'engrenage.

![](./images/planetary%20gear%201.jpg)

![](./images/planetary%20gear%202.jpg)


#### Deuxième prototype d'engrenage

Le deuxième prototype imprimé est le suivant. Il a entièrement été designé sur OpenSCAD. Pour mieux visualiser les choses, on a utilisé [Gear Generators](https://geargenerator.com/beta/#8o7@xcKzTcKzTcKzTc5XB7vY5ouCtjMLtA27WaRjgDcwi4DbIxgNCOwjXMNzm1nA$h2JJHWWziOGXgIaKzlPcvISKGBxyrmmfApd3ExP4bBzqMKh1NB@u9JKlbxoCou0PoY2FFs5SxCTHWOWG1$h2jISpotfiLSzhWcC0y2). 

Comme on peut le voir, le but était de créer une série d'engrenages suivis les uns après les autres, le premier tournant très lentement, et le dernier très vite. Le premier engrenage serait alors lié au poids qui tombe lentement, et le dernier serait relié à la dynamo qui est censé tourner très vite pour génerer assez de puissance. 

![](./images/geargenerator.jpg)

Les voilàs imprimés. On les a fixés sur une planche en faisant tout simplement des trous dans la planche, et en insérant les gears dans des vis fixées à la plance. 

![](./images/engrenages%20THéo.jpg)

Voici une vidéo qui illustre le fonctionnement (oui c'est cool ) : 

<video width="320" height="240" controls>
  <source src="../images/videoproto1.mp4" type="video/mp4">
</video>

*PS : Les boulons que l'on peut appercevoir sur la vidéo avaient juste pour but de mettre du poids sur les engrenanges pour stabiliser le mécanisme.*


Nous avons décidé que d'un point de vue du modèle (et son intégration dans les différents composants de la lampe), le système d'engrenages de Theo étais le plus facile à implimenter, malgré le fait que la planetary gear system soit un bon moyen d'avoir une vitesse faible d'un coté et une grande vitesse de l'autre.

Les problèmes de ce protoype sont les suivants : 

- Trop légers
- Trops fins
- Pour que le mécanisme fonctionne, il faut que les engrenanges se supperposent en hauteur, il fallait donc imprimer des "spacers", qui vont espacer chaque engrenange entre eux.



#### Troisième prototype

On a donc fait un troisième prototype : 

![](./images/proto2.jpg)

On voit bien que les problèmes du deuxième prototypes ont été fixés. Ils sont plus épais, plus massif, et des "spacers", qui permettent du surélever chaque spacers par rapport à l'autre ont été créés.

Ce prototype a été imprimé en plus grand pour pouvoir tester son utilisation dans le système complet.
Voilà à quoi il ressemble : 

![](./images/proto4.jpg)

Malheureusement, il est loin de fonctionner efficacement. Il a en effet beaucoup de frottements, et de jeu, qui sont provoqués par l'allignement imparfait des engrenages et des résidus de plastique se trouvant sur les dents.  


## <span style="color:orange;">3. Changement de cap : Abandon de l'idée de la Gravity Lamp</span>

Les premières manpulations du groupe avaient donc pour but de connecter l'entierté des composants (pour la gravity lamp), mais en revoyant les calculs du dimensionnement avec Denis, on s'est rendu compte que ce système ne pouvait allumer la LED que pendant 5 min.

En effet, si on reprend la formule de l'énergie potentielle, celle-ci est donnée par E=mgh.Avec un poids de 1kg, g=10m/s^2 et une hauteur de 1m, on a: E=1 * 10 * 1=10J .Comme 1W=1J/s, 10J=10s.Cela voudrais donc dire que 1kg et 1m donnent 10s de lumière.Notre estimation de poids maximal a mettre est de 20kg, et pour une hauteur de 1m50 maximum, on pourrais donc allumer la LED que pendant 5minutes, ce qui n'est pas très pratique si l'on veut travailler(car on devrais soulever le poids toutes les 5minutes).

Sachant que la probabilité qu'une personne ne soit pas dérangée par le fait de remonter le poids toutes les 5 min était plutot faible, on a été contraint d'abandonner cette voie.

On a alors compris qu'il serait plus intéressant de détourner l'utilisation d'un objet courant pour qu'il résolve notre problème plutot que de créer une solution de toute pièce. Or, un objet remplit déjà presque tous les critères pour solutionner nottre projet.
L'idée est de reprendre une lampe dynamo déjà existante telle que celle-ci : ![](image/../images/lampe.jpg) L'objectif étant d'illuminer un bureau pour quelqu'un qui travaille, ou qui lit, allumer la LED avec les mains n'est pas une idée brillante...
C'est donc là que l'idée d'actionner la dynamo avec les pieds a surgi. Comment ? En appuyant sur une pédale qui actionnerait le petit triangle de la lampe torche.

Ainsi, les mains de l'utilisateurs sont libres pour le travail, lecture, peu importe, et la lumière peut être actionnée avec le pied à l'image d'une pédale d'un batteur qui fait de la batterie, ou d'un pianiste.
Le pied actionnerait donc le triangle latéral de la lampe de poche grâce à une pédale. Etant donné que l'on veut illuminer le bureau, il suffit de démonter la lampe, allonger les fils de telle sorte à ce qu'ils soient situés en hauteur pour illuminer le bureau. 

Un peu comme ca : 

![](./images/imagechatgpt.jpg) 


Pour que la puissance de la LED reste constante, il faut donc actionner en permanence le triangle latéral de la lampe. Pour cela, il faut créer une boite avec une pédale relié a un système facilement actionable en mettant un peu de pression avec son pied.

Cette solution permetterait à l'utilisateur d'avoir ses mains libres, tout en voyant ce qu'il fait. 

La solution reste dans le cadre d'une production de lumière de manière écologique, car l'énergie est générée par l'utilisateur sous forme mécanique, et non par des énergies fossiles.
De plus, elle n'est pas dépendante du réseau électrique,ce qui répond parfaitement à la problématique du Liban.

Enfin, le fait de faire des mouvements répétitifs avec le pied pourrait canaliser l'énergie et résulter a une meilleure concentration.

Ce changement de solution nous a fait réalisé a quel point les calculs a faire avant le prototypage étaient important. La lecon qu'on en retire est dvérifier les calculs entre nous et par un mentor, pour être sur de ne pas s'emballer dans un projet voué à l'échec (ou n'ayant pas le résulat voulu).

## <span style="color:orange;">4. Hackaton : On rentre dans le vif du sujet </span>

Ce changement de cpa étant effectué juste avant le Hackaton, nous avons pu profiter de ces deux jours intenses pour développer rapidement la nouvelle solution trouvée.
### <span style="color:magenta;">4.1 Prototypage de la boite/ pédale</span>

Lors du hackathon, nous avons modélisé les pièces pour la boite et la pédale...Nous avons fait une boite possédant deux trous, dans lequel on ferait passer un cylindre avec une pédale...

#### 1.Premier prototype:en bois: 

![](./images/Proto-bois.jpeg)

Notre premier reflexe a été de faire un prototype en bois pour voir comment la pédale/notre prototype s’agencait dans l’espace.En le faisant, cela nous a permis de voir que la hauteur du trou dans la pédale(par lequel passe le cylindre maintenant la pédale en place), etais plutôt importante car si on la mettais trop basse, la pédale heurtait le sol(comme on le voit dans la photo) et il n'y avait pas de place pour insérer la lampe de poche. Cela nous a aussi permis de nous rendre compte que la longueur du cylindre étais importante(pour traverser toute la largeur de la boite).

#### 2.Deuxième prototype:en 3D:

Après la finalisation des protytpes sur openscad, nous les avons imprimés et en voici les résultats:

![](./images/Boite%20pédale%20version%201.jpg)
![](./images/Proto-3D.jpeg)


On s'est rendu compte que dans ce modèle, l'épaisseur des parois était trop fine(lors de l'actionnement de la lampe, la pression serait peut etre trop grande et pourrais casser les parois)...La modélisation de la boite étais faite sans avoir la lampe de poche(comme on avais changé de projet trois jours avant le hackathon, et que le seul moyen de commander la lampe etais sur internet, le colis pouvais etre livré au plus tot le jour du hackathon, nous empéchant de bien visualiser comment mettre la lampe de poche dans la boite).Les trous aussi étaient trop bas car nous avions oubliés que le triangle latéral étais au dessus de la lampe.Il faut donc mettre les trous plus haut afin de pouvoir actionner le triangle latéral, comme on le voit dans la photo ci-dessous:

![](./images/lampe%20de%20poche-pédale.jpg)

#### 3.Dernier prototype:

En réfléchissant, on c'est rendu compte que l'utilisation d'une imprimante 3D pour faire une boite étais un gaspillage de fil PLA, on a donc opté pour faire un boitier en bois.Cela nous a également permis de régler le problème de rigidité de la structure...


Pour réaliser la boite, la première étape est de découper l'ensemble des planches selon les dimensions de la lampe de poche:

![](./images/Plan-bois.jpeg)
![](./images/Decoupe-bois.jpeg)

Une fois assemblée, nous avons pu tester que toutes les dimensions étaient bonnes en insérant temporairement la lampe de poche.

![](./images/Bois-construits.jpeg)
![](./images/Bois-construit.png)

La pédale en bois était enfin prête à étre utilisée.


La seconde partie du projet est le démontage de la lampe de poche. En effet, le but est de séparer la source lumineuse (les LEDs) du système de dynamo. Pour cela, dévisser les vis de maintient et ouvrir le boitier suffit à exposer tout le méchanisme. On a déconnecté l'interupteur intégré à la lampe de poche et identifier les connections faites entre la dynamo et les LEDs.  L'utilisation d'une dynamo pose deux soucis principaux.

Premièrement, comme la dynamo fait des mouvements d’allers et retours, elle produit un courant dont le sens change à chaque fois. Pour régler ça on utilise un [pont de diodes](https://fr.wikipedia.org/wiki/Pont_de_diodes). Ce dernier permet de transformer le courant alternatif en courant continu en filtrant le passage des électrons dans un sens seulement pour chaque borne.

Deuxièmement, la source d'alimentation n'est pas du tout stable ni continue. Pour éviter de produire une lumière qui clignoterait à chaque variation de l'alimentation, nous ajoutons au circuit une capacité. Celle ci va stocker un peu d'énergie et la libérer quand l'alimentation sera plus faible que la tension stockée. Elle permet donc de compenser les pertes de puissance d el'alimentation et donc de garder une illumination constante au niveau des LEDs. 

Le pont de diode est le carré noir et la capacité est le cyclindre orange et noire sur la photo de notre circuit électronique. La distance séparant la dynamo des ampoules LEDs n'est pas une limitte dans notre cas d'application. Les fils électriques classiques peuvent ête soudés pour agrandir cette distance si besoin. Bien que ces deux ajouts sont élémentaires,aucune personne de notre groupe n'avait d'éxpérience en électronique avant cela. Nous avons donc beaucoup appris dans la conception de ce petit circuit.


![](./images/electronique.jpeg)

Le montage final est représenté dans l'image suivante. On voit que le boitier est imposant car il doit être assez solide pour résister à l'appui à répétition. La partie contenant la LED est très légère pour sa part et peut donc facilement être installée là ou l'éclairage est le plus optimal pour travailler.


![](./images/final1.jpeg)
![](./images/final2.jpeg)

## <span style="color:orange;">5. Conclusion</span>

Ce projet nous aura beaucoup appris. Premièrement en nous rendant plus conscient de notre manière de travailler en groupe et des dynamiques mises en places. Ensuite nous avons appris à développer une problématique en se concentrant sur un cadre spécifique. L'avantage de ce cadre est de nous obliger à rester créatif mais en restant dans le domaine du possible. En essayent d'améliorer un peu un système existant, on peut arriver à totalement transformer son utilisation initiale. Nous avons aussi appris à travailler dur sur un projet pour finalement l'abandonner à cause d'une erreur initiale non vérifiée et malgré tout, conserver notre problématique pour trouver un autre moyen de la résoudre. Enfin, nous nous sommes familiarisés avec les notions de base en électronique et nous avons réussi à développer un prototype fonctionnel en un temps très court. Nous savons que nous pouvons encore améliorer notre résultat final, nottament en utilisant une découpeuse laser pour améliorer la précision des pièces et le design du boitier à pédale. Cependant, aucun de nous n'avait suivi cette formation et il était impossible de s'inscrire pour apprendre à utiliser cette machine dans le temps imparti. Nous sommes donc fiers du travail que nous avons fourni et de notre évolution tout au long du projet ainsi que de notre capacité à rebondir sur un échec pour en sortir avec une solution plus adéquate.